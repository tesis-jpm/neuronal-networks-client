# Neuronal Networks Client

## Pasos para instalacion y ejecucion del sistema:
**IMPORTANTE: Verificar que todas las máquinas (servidores y clientes) estén en la misma red para poder acceder al dataset.**

1. Instalación de python (versiones de Python que soporta: 3.10.6 y 3.7.9).
2. Instalación de las librerías *tensorflow* y *psutil* con pip.
3. Verificar el archivo de configuración.
4. Ejecutar el archivo *tensorflowclientpy* o *tensorflowclientpython* dependiendo de cómo se le llama al python instalado.

## Archivo de configuración "*config.json*"
1. **broadcast**
   - **port:** valor entre 0 y 65535. Puerto en el que el cliente recibirá el broadcast una vez que el servidor ha iniciado, ej.: 5005.
2. **resources**
   - **max_cpu_percentage:** valor entre 0 y 100. Porcentaje máximo de cpu que permitirá, en caso de que sobrepase el cliente matará el proceso de tensorflow.
   - **max_memory_percentage:** valor entre 0 y 100. Porcentaje máximo de memoria ram que permitirá, en caso de que sobrepase el cliente matará el proceso de tensorflow.
   - **status_check_interval:** valor mínimo de 0. Intervalo de tiempo que esperará para realizar el análisis de los recursos.
   - **timeout_duration:** valor mínimo de 0. Una vez el proceso de tensorflow se haya detenido por superar alguno de los límites de recursos, el tiempo que esperará para volver a revisar si es que ya se encuentran disponibles.
3. **logfile:** archivo en el que se guardará el log del cliente.