import psutil
from config import Config


class Monitor:
    def __init__(self):
        config = Config()
        self.maximum_usable_cpu_percentage = config.get("resources.max_cpu_percentage")
        self.maximum_usable_memory_percentage = config.get("resources.max_memory_percentage")

    def get_memory_percentage_used(self):
        return psutil.virtual_memory()[2]

    def get_cpu_percentage_used(self):
        return psutil.cpu_percent(1)

    def is_available(self):
        # check other config for percentage
        cpu_limit = self.maximum_usable_cpu_percentage
        memory_limit = self.maximum_usable_memory_percentage
        cpu_in_range = self.get_cpu_percentage_used() <= cpu_limit
        memory_in_range = self.get_memory_percentage_used() <= memory_limit
        return cpu_in_range and memory_in_range
