import os
import socket
import json
import multiprocessing
from datetime import datetime

import tensorflow as tf
from threading import Thread
from config import Config
from time import sleep
from monitor import Monitor
import logging

# processes list
processes = []

class Client:
    def __init__(self):
        os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
        # config class
        self.config = Config()
        self.BROADCAST_PORT = self.config.get("broadcast.port")
        self.logfile = self.config.get("logfile")
        root_logger = logging.getLogger()
        root_logger.setLevel(logging.INFO)
        handler = logging.FileHandler(self.logfile, 'w', 'utf-8')
        root_logger.addHandler(handler)


    # Wait for the server to send the broadcast and manage the message
    def wait_for_broadcast(self):
        # wait for a request
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        sock.bind(("0.0.0.0", self.BROADCAST_PORT))
        stop = False
        timeout_duration = self.config.get("resources.timeout_duration")
        monitor = Monitor()
        while not stop:
            logging.info(f"{datetime.now()} Listening on {self.BROADCAST_PORT} port on lookout for work.")
            # received a communication from the broadcast
            received_data, addr = sock.recvfrom(1024)
            # load message as a json
            received_data = json.loads(received_data.decode("utf-8"))
            logging.info(f"{datetime.now()} Command received: {received_data}")
            if any(x in received_data for x in ["stop", "restart", "ip"]):
                self.kill_process("worker")
                self.kill_process("ps")
                if "stop" in received_data:
                    # stop process
                    stop = True
                elif "ip" in received_data:
                    # check if there are enough resources to start the training
                    if monitor.is_available():
                        # get server's ip and port
                        server_ip = received_data["ip"]
                        server_port = received_data["port"]
                        logging.info(f"{datetime.now()} Server info is ip: {server_ip} port: {server_port}")
                        self.send_connection_data_to_server(server_ip, server_port)
                    else:
                        logging.info(f"{datetime.now()} Resources over limit, not joining training.")
                        sleep(timeout_duration)
        # close the listening port
        sock.close()

    # Send data required for the connection to this worker/ps to the server
    def send_connection_data_to_server(self, server_ip, server_port):
        # gets port for communication
        listening_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        listening_sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        listening_sock.bind(('', 0))
        # gets ports for tensorflow (one for ps and another for worker)
        tensor_sock1 = socket.socket()
        tensor_sock2 = socket.socket()
        tensor_sock1.bind(('', 0))
        tensor_sock2.bind(('', 0))
        # store all ports on variables
        port_number = listening_sock.getsockname()[1]
        tensor_port1 = tensor_sock1.getsockname()[1]
        tensor_port2 = tensor_sock2.getsockname()[1]

        # start listening for a server response with the nodes and work info
        r_thread = Thread(
            target=self.wait_for_server_response,
            args=(tensor_sock1, tensor_sock2, listening_sock,)
        )
        r_thread.start()
        # get client ip of this machine
        host_name = socket.gethostname()
        ip_addr = socket.gethostbyname(host_name)

        # send data to server
        logging.info(f"Sending data")
        msg = {
            'ip': ip_addr,
            'port': port_number,
            'tensorflow_port1': tensor_port1,
            'tensorflow_port2': tensor_port2
        }
        logging.info(f"-> {msg}")
        data = json.dumps(msg)
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        sock.sendto(data.encode('utf-8'), (server_ip, server_port))
        logging.info(f"{datetime.now()} Data correctly sent to the server.")
        sock.close()

    # Wait for the server to respond with de cluster info and start training
    def wait_for_server_response(self, tensor_sock1, tensor_sock2, listening_sock):
        # get server response
        received_data = listening_sock.recvfrom(1024)[0].decode("utf-8")
        cluster_json = json.loads(received_data)
        logging.info(f"{datetime.now()} Received response from server.")
        logging.info(f"<- {received_data}")
        for task in cluster_json['tasks']:
            work_json = {
                'cluster': cluster_json['cluster'],
                'task': task
            }
            logging.info(f"{datetime.now()} Start job with config.")
            logging.info(f"{work_json}")
            process = multiprocessing.Process(
                target=self.start_tensorflow,
                args=(work_json,)
            )
            processes.append((task, process))

        # close port that was waiting the response
        listening_sock.close()
        # close port that was keep open in case it was taken by another process in the communication time
        tensor_sock1.close()
        tensor_sock2.close()
        # start tensorflow on second process
        for process in processes:
            process[1].start()

        # start checking resources status
        r_thread = Thread(target=self.check_resource_status, args=())
        r_thread.start()

        # wait for all tensorflow processes currently running to finish to end the client
        for process in processes:
            process[1].join()

    # Use this to kill a worker (default) or a ps
    def kill_process(self, process_type="worker"):
        to_remove = []
        for process in processes:
            # if it is a worker, kill it :evil:
            if process[0]['type'] == process_type:
                logging.info(f"killing #{process_type}")
                to_remove.append(process)
                process[1].kill()
        for process in to_remove:
            processes.remove(process)

    # start tensorflow client with a given cluster info
    def start_tensorflow(self, cluster_conf):
        logging.info(f"{datetime.now()} Running tensorflow client.")
        os.environ["TF_CONFIG"] = json.dumps(cluster_conf)

        cluster_resolver = tf.distribute.cluster_resolver.TFConfigClusterResolver()
        os.environ["grpc_fail_fast"] = "use_caller"
        os.environ["GRPC_FAIL_FAST"] = "use_caller"

        server = tf.distribute.Server(
            cluster_resolver.cluster_spec(),
            job_name=cluster_resolver.task_type,
            task_index=cluster_resolver.task_id,
            protocol="grpc",
            start=True)
        server.join()

    # check resources statys and kill worker in case it is over the limit
    def check_resource_status(self):
        sleep(20)
        keep_listening = True
        status_check_interval = self.config.get("resources.status_check_interval")
        monitor = Monitor()
        while keep_listening:
            if not monitor.is_available():
                logging.info(f"{datetime.now()} Resources are not available: server stopped.")
                self.kill_process("worker")
                self.kill_process("ps")
                keep_listening = False
            sleep(status_check_interval)

    def clear_log(self):
        path = os.getcwd() + '\\info.log'

        try:
            open(path, 'w').close()
        except IOError:
            print('Failure')


if __name__ == "__main__":
    client = Client()
    client.clear_log()
    client.wait_for_broadcast()
